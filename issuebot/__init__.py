import binascii
import collections
import configparser
import git
import github
import gitlab
import gitlab.v4.objects
import glob
import hashlib
import json
import os
import re
import requests
import shutil
import subprocess
import sys
import tempfile
import traceback
import yaml

from colorama import Fore, Style
from datetime import datetime
from distutils.version import LooseVersion
from fdroidserver import common, metadata, net
from textwrap import dedent
from urllib.parse import parse_qs, urlparse

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader

HEADERS = {'User-Agent': 'F-Droid Issuebot'}
REQUESTS_TIMEOUT = 120

ISSUEBOT_API_DIR = 'public/issuebot/'  # trailing slash makes it work with .startswith()

IZZYSOFT_PATTERN = re.compile(r'''.*<a [^>]* href=['"](.+)['"][^>]*>Download.*''')
JAVA_PACKAGENAME = (
    r'''(?:[a-zA-Z]+(?:\d*[a-zA-Z_]*)*)(?:\.[a-zA-Z]+(?:\d*[a-zA-Z_]*)*)+'''
)
GIT_PATTERN = re.compile(
    r'http[s]?://(github.com|gitlab.com|bitbucket.org|git.code.sf.net|codeberg.org|framagit.org)/[\w.-]+/[\w.-]+'
)
GPLAY_PATTERN = re.compile(
    r'http[s]?://play\.google\.com/store/apps/details\?id=%s' % JAVA_PACKAGENAME
)
APPLICATION_ID_PATTERN = re.compile(
    r'''(?:APPLICATION|app|PACKAGE) *(?:ID|NAME|)[:=\s]+['"]?(%s)''' % JAVA_PACKAGENAME,
    re.IGNORECASE | re.DOTALL,
)
APK_DOWNLOAD_URL_PATTERN = re.compile(
    r'https?://[^ /]+/[^ :]+\.apk', flags=re.IGNORECASE
)
# BCP47 language tags used by Fastlane/Triple-T
BCP47_LOCALE_TAG_PATTERN = re.compile(r"[a-z]{2,3}(-([A-Z][a-zA-Z]+|\d+|[a-z]+))*")

PROCESSED = 0
applicationIds_data = dict()
sourceUrls_data = dict()


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        return super().default(obj)


class IssuebotModule:
    reply = {'emoji': set(), 'labels': set(), 'report': '', 'reportData': dict()}

    def __init__(self):
        self.application_id = os.getenv('ISSUEBOT_CURRENT_APPLICATION_ID', '').strip()
        if self.application_id:
            self.reply['applicationId'] = self.application_id
        job_id, issue_id = get_job_issue_ids()
        self.job_id = int(job_id)
        self.issue_id = int(issue_id)
        cwd = os.getcwd()
        self.base_dir = cwd

        found = []
        found += sorted(glob.glob(os.path.join(cwd, 'unsigned', '*.apk')))
        found += sorted(glob.glob(os.path.join(cwd, 'repo', '*.apk')))
        self.apkfiles = []
        common.config = {}  # hack to deal with this vestigal requirement
        for apkfile in found:
            apk_id = get_apk_id(apkfile)
            if not apk_id:
                moved = str(apkfile) + '?'
                print('Moved to', moved)
                shutil.move(str(apkfile), moved)
            elif get_apk_id(apkfile)[0] == self.application_id:
                self.apkfiles.append(apkfile)

        self.source_url = os.getenv('ISSUEBOT_CURRENT_SOURCE_URL', '').strip()
        if self.source_url:
            self.reply['sourceUrl'] = self.source_url
        self.source_dir = os.path.join(cwd, 'build', self.application_id)
        self.metadata_file = os.path.join(cwd, 'metadata', self.application_id + '.yml')
        if 'PERSONAL_ACCESS_TOKEN' in os.environ:
            self.gitlab_api_key = os.getenv('PERSONAL_ACCESS_TOKEN')
        if 'GITHUB_TOKEN' in os.environ:
            self.github_api_key = os.getenv('GITHUB_TOKEN')
        if 'VIRUSTOTAL_API_KEY' in os.environ:
            self.virustotal_api_key = os.getenv('VIRUSTOTAL_API_KEY')

    def add_label(self, labels):
        if isinstance(labels, str):
            self.reply['labels'].add(labels)
        else:
            for label in labels:
                self.reply['labels'].add(label)

    def is_ci(self):
        """check whether running in a CI environment as root"""
        return 'CI' in os.environ and os.getuid() == 0

    def run_gradle_plugin(self, name, task=None):
        home = os.getenv('GRADLE_USER_HOME')
        if not home:
            home = os.path.join(os.getenv('HOME'), '.gradle')
        plugin = os.path.join(home, '%s.gradle' % name)
        if os.path.exists(plugin):
            cmd = [self.gradlew, '--init-script', plugin]
            if task:
                cmd.append(task)
            p = run_cli_tool(cmd, self.source_dir)
            print(p.stdout.decode())

    def get_build_gradle_with_android_plugin(self):
        """get all build.gradle(.kts) files with the com.android.application plugin"""
        paths = common.get_all_gradle_and_manifests(self.source_dir)
        self.gradle_subdir = str(common.get_gradle_subdir(self.source_dir, paths))
        g = glob.glob(
            os.path.join(self.source_dir, self.gradle_subdir, 'build.gradle*')
        )
        if not g:
            return ''
        self.add_label('gradle')
        gradlew_path = os.path.join(self.source_dir, 'gradlew')
        if os.path.exists(gradlew_path):
            os.chmod(gradlew_path, 0o700)  # sometimes devs forget...
            self.gradlew = './gradlew'
        else:
            self.gradlew = '/usr/bin/gradle'
        return g

    def get_gitlab_api(self):
        return gitlab.Gitlab(
            'https://gitlab.com', api_version=4, private_token=self.gitlab_api_key
        )

    def calc_apk_checksums(self):
        """Calculate MD5, SHA1, and SHA256 for all APKs and cache them"""
        if not hasattr(self, 'apk_checksums'):
            self.apk_checksums = collections.OrderedDict()
            for apk in self.apkfiles:
                md5 = hashlib.md5()  # nosec used as backwards compatible ID
                sha1 = hashlib.sha1()  # nosec used as backwards compatible ID
                sha256 = hashlib.sha256()
                with open(apk, 'rb') as f:
                    while True:
                        t = f.read(16384)
                        if len(t) == 0:
                            break
                        md5.update(t)
                        sha1.update(t)
                        sha256.update(t)
                self.apk_checksums[os.path.basename(apk)] = {
                    'md5': binascii.hexlify(md5.digest()).decode(),
                    'sha1': binascii.hexlify(sha1.digest()).decode(),
                    'sha256': binascii.hexlify(sha256.digest()).decode(),
                }
            self.reply['reportData']['apkChecksums'] = self.apk_checksums

    def apkfiles_required(self):
        """Run this first for modules that require APK files to analyze"""
        if self.apkfiles:
            return True
        else:
            print('Skipping, no APK files found')
            self.write_json()
            return False

    def get_current_commit_id(self):
        if not os.path.isdir(os.path.join(self.source_dir, '.git')):
            return
        commit_id = (
            subprocess.check_output(
                ['git', '-C', self.source_dir, 'describe', '--always', '--tags']
            )
            .decode()
            .strip()
        )
        self.reply['commitId'] = commit_id
        return commit_id

    def get_current_commit_id_url(self, commit_id=None):
        if not os.path.isdir(os.path.join(self.source_dir, '.git')):
            return
        if commit_id is None:
            commit_id = self.get_current_commit_id()
        if re.compile(
            r"https://(github\.com|gitlab\.com|framagit\.org|git.sr.ht)/"
        ).match(self.source_url):
            tag_path = 'tree'
        elif re.compile(r"https://(codeberg\.org|notabug\.org|bitbucket\.org)/").match(
            self.source_url
        ):
            tag_path = 'src'
        else:
            for tag_path in ('tree', 'src'):
                tag_url = os.path.join(self.source_url, tag_path, commit_id)
                try:
                    r = requests_head(tag_url)
                    r.raise_for_status()
                    return tag_url
                except Exception as e:
                    print(e)
            else:
                return
        return os.path.join(self.source_url, tag_path, commit_id)

    def get_source_scanning_header(self, text):
        """Generate the HTML for a header of a module that scans the source code"""
        return '<h3>%s @ <a href="%s" target="_blank"><tt>%s</tt></a></h3>' % (
            text,
            self.get_current_commit_id_url(),
            self.get_current_commit_id(),
        )

    def get_source_url(self, f):
        repo = git.repo.Repo(self.source_dir)
        commit_id = binascii.hexlify(bytearray(repo.head.commit.binsha)).decode()
        self.reply['commitId'] = commit_id
        return os.path.join(self.source_url, 'blob', commit_id, f)

    def write_json(self):
        with open(
            os.path.join(get_json_dir(), os.path.basename(sys.argv[0]) + '.json'), 'w'
        ) as fp:
            json.dump(
                self.reply,
                fp,
                indent=2,
                sort_keys=True,
                ensure_ascii=False,
                cls=Encoder,
            )

    def write_local_test_report(self):
        with open(os.path.join(self.base_dir, 'test_report.html'), 'w') as fp:
            fp.write(
                '<html><head><meta http-equiv="refresh" content="10"></head><body>'
            )
            fp.write(self.reply['report'])
            fp.write('</body></html>')


def get_job_issue_ids():
    return (
        os.getenv('CI_JOB_ID', str(int(datetime.utcnow().timestamp()))).strip(),
        os.getenv('ISSUEBOT_CURRENT_ISSUE_ID', '0').strip(),
    )


def parse_git_urls_from_description(description):
    """parse valid git URLs from a block of text

    This maintains the order in which the URLs were found to give
    priority to the URLs which the user has written first in the
    description.  Duplicates are not included.  The position in the
    list is based on when the URL was first seen.

    """

    with open('data/gitlab-reserved-names.json') as fp:
        gitlab_reserved_names = json.load(fp)
    with open('data/github-reserved-names.json') as fp:
        github_reserved_names = json.load(fp)

    git_urls = []
    for m in GIT_PATTERN.finditer(description):
        url = m.group(0)
        if url.startswith('http://'):
            url = 'https://' + url[7:]
        if url.endswith('.git'):
            url = url[:-4]
        # skip fdroid repos and GitLab/GitHub reserved group names
        first_segment = urlparse(url).path.split('/')[1]
        if (
            (
                url.startswith('https://gitlab.com/')
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://codeberg.org/')  # a GitLab instance
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://framagit.org/')  # a GitLab instance
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + gitlab_reserved_names
            )
            or (
                url.startswith('https://github.com/')
                and first_segment
                not in ['', 'fdroid', 'f-droid'] + github_reserved_names
            )
        ):
            if url not in git_urls:
                git_urls.append(url)
    return git_urls


def get_apk_id(apkfile):
    """Wrapper for fdroidserver.common.get_apk.id() to handle setup"""
    # TODO check whether the upstream one works without this wrapper
    common.config = dict()
    try:
        return common.get_apk_id(str(apkfile))
    except Exception as e:
        print(
            Fore.YELLOW
            + 'WARNING: bad APK "%s":\n%s: %s' % (apkfile, e.__class__.__name__, e)
            + Style.RESET_ALL
        )


def download_apks_urls_in_description(description):
    """Search descrition for APK urls and download any that are found"""
    discovered_appids = set()
    if description:
        for url in APK_DOWNLOAD_URL_PATTERN.findall(description):
            apkfile = download_file(url)
            apk_id = None
            if os.path.exists(apkfile):
                apk_id = get_apk_id(apkfile)
            if apk_id and apk_id[0]:
                discovered_appids.add(apk_id[0])
    return discovered_appids


def get_json_dir():
    project_dir = os.getenv('CI_PROJECT_DIR', os.getcwd())
    job_dir, issue_id = get_job_issue_ids()
    d = os.path.join(project_dir, ISSUEBOT_API_DIR, job_dir, issue_id)
    os.makedirs(d, exist_ok=True)
    return d


def download_file(url, dldir='repo'):
    try:
        net.download_file(url, dldir=dldir)
    except requests.exceptions.HTTPError as e:
        print(e)
    return os.path.join(dldir, os.path.basename(url))


def read_properties(path):
    with open(path, errors="surrogateescape") as fp:
        text = fp.read()
    config = configparser.ConfigParser()
    config.read_string("[DEFAULT]\n" + text)  # fake a INI file
    return text, config["DEFAULT"]


def get_metadata_file(appid):
    """Return the relative path to the metadata file for the given Application ID"""
    return 'metadata/%s.yml' % appid


def requests_get(url):
    return requests.get(url, headers=HEADERS, timeout=REQUESTS_TIMEOUT)


def requests_head(url):
    return requests.head(url, headers=HEADERS, timeout=REQUESTS_TIMEOUT)


def create_dummy_fdroid_repo():
    os.makedirs('build', exist_ok=True)
    os.makedirs('metadata', exist_ok=True)
    os.makedirs('repo', exist_ok=True)
    with open('fdroid-icon.png', 'w') as fp:
        fp.write('')
    with open('config.yml', 'w') as fp:
        d = {
            'allow_disabled_algorithms': True,
            'make_current_version_link': False,
            'repo_pubkey': "deadbeef",
            'androidobservatory': False,
        }
        VIRUSTOTAL_API_KEY = os.getenv('VIRUSTOTAL_API_KEY')
        if VIRUSTOTAL_API_KEY:
            d['virustotal_apikey'] = VIRUSTOTAL_API_KEY
        yaml.dump(d, fp)


def get_apk_from_github(url, labels):
    if url.startswith('https://github.com/'):
        try:
            token = os.getenv('GITHUB_TOKEN')
            g = github.Github(token)
            repo = g.get_repo('/'.join(url.split('/')[3:5]))
            release = repo.get_latest_release()
            for asset in release.get_assets():
                if asset.browser_download_url.endswith('.apk'):
                    print('Downloading from %s/releases/latest' % url)
                    download_file(asset.browser_download_url)
                    labels.add('in-github-releases')
        except github.GithubException:
            print_exc_flush('get_apk_from_github')


def get_apk_from_izzysoft(appid, labels):
    url = 'https://apt.izzysoft.de/fdroid/index/apk/' + appid
    try:
        r = requests_head(url)
        r.raise_for_status()
        labels.add('in-izzysoft')
        r = requests_get(url)
        r.raise_for_status()
        m = IZZYSOFT_PATTERN.search(r.text)
        if m:
            dl_url = 'https://apt.izzysoft.de' + m.group(1)
            print('Downloading', dl_url)
            download_file(dl_url)
    except requests.exceptions.RequestException as e:
        print('get_apk_from_izzysoft', e)


def run_per_app(local_issue_id, appid):
    os.environ['ISSUEBOT_CURRENT_ISSUE_ID'] = str(local_issue_id)
    os.environ['ISSUEBOT_CURRENT_APPLICATION_ID'] = appid
    emoji = set()
    labels = set()
    report = ''
    get_apk_from_huawei_app_gallery(appid, labels)
    get_apk_from_izzysoft(appid, labels)
    get_apk_from_google_play(appid, labels)
    for version in get_available_apk_versions_from_apkpure(appid):
        get_apk_from_apkpure(appid, labels, version)
    for f in get_modules():
        print(
            '\n===================================================================\n',
            'Running',
            f,
        )
        reply_file = os.path.join(get_json_dir(), os.path.basename(f) + '.json')
        os.environ['ISSUEBOT_CURRENT_REPLY_FILE'] = reply_file
        try:
            p = subprocess.run([f], timeout=1800)
            if p.returncode == 0:
                with open(reply_file) as fp:
                    data = yaml.load(fp, Loader=SafeLoader)
                emoji.update(data.get('emoji', []))
                labels.update(data.get('labels', []))
                report += data.get('report', '<!-- %s returned no report -->' % f)
        except (
            FileNotFoundError,
            subprocess.TimeoutExpired,
            yaml.scanner.ScannerError,
            yaml.reader.ReaderError,
        ):
            print_exc_flush(f)

    print(
        '\n===================================================================\n',
        'results:',
    )
    print(sorted(emoji))
    print(sorted(labels))
    with open(os.path.join(get_json_dir(), appid + 'report.html'), 'w') as fp:
        fp.write(
            dedent(
                """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                <title>report</title>
                <meta http-equiv="refresh" content="5">
                </head>
                <body>
                <h1>{appid}</h1>
                """
            ).format(appid=appid)
        )
        if emoji:
            fp.write('<h3>emoji</h3><ul>')
            for e in sorted(emoji):
                fp.write('<li>{}</li>'.format(e))
            fp.write('</ul>')
        if labels:
            fp.write('<h3>labels</h3><ul>')
            for label in sorted(labels):
                fp.write('<li>{}</li>'.format(label))
            fp.write('</ul>')
        fp.write(report)
        fp.write('</body></html>')

    return emoji, labels, report


def get_available_apk_versions_from_apkpure(appid):
    """Get list of available versions, sorted oldest to newest"""
    if not shutil.which('apkeep'):
        print('apkeep not installed, skipping APKPure')
        return []
    apkeep_list_versions = re.compile(r'\|[^|]+')
    versions = set()
    p = subprocess.run(
        ['apkeep', '--app', appid, '--list-versions'], capture_output=True
    )
    if p.returncode == 0:
        m = apkeep_list_versions.search(p.stdout.decode())
        if m:
            for v in m.group()[1:].split(','):
                versions.add(v.strip())
    try:
        return sorted(versions, key=LooseVersion)
    except TypeError:
        return sorted(versions)


def get_apk_from_google_play(appid, labels, dldir='repo'):
    """Use apkeep and credentials to download from Google Play

    https://github.com/EFForg/apkeep/blob/master/USAGE-google-play.md

    """
    if not shutil.which('apkeep'):
        print('apkeep not installed, skipping Google Play')
        return
    username = os.getenv('GOOGLE_PLAY_USERNAME')
    if not username:
        print('GOOGLE_PLAY_USERNAME not set, skipping Google Play')
        return
    password = os.getenv('GOOGLE_PLAY_PASSWORD')
    if not password:
        print('GOOGLE_PLAY_PASSWORD not set, skipping Google Play')
        return
    config = configparser.ConfigParser()
    config['google'] = {'username': username, 'password': password}
    with tempfile.NamedTemporaryFile(mode='w') as fp:
        config.write(fp)
        fp.flush()
        subprocess.run(
            [
                'apkeep',
                '--download-source',
                'google-play',
                '--ini',
                fp.name,
                '--app',
                appid,
                dldir,
            ]
        )
    if os.path.exists(os.path.join(dldir, appid + '.apk')):
        labels.add('in-google-play')


def get_apk_from_apkpure(appid, labels, version=None, dldir='repo'):
    if not shutil.which('apkeep'):
        print('apkeep not installed, skipping APKPure')
        return
    dlname = appid
    if version:
        dlname += '@' + version
    subprocess.run(['apkeep', '--download-source', 'apk-pure', '--app', dlname, dldir])
    if os.path.exists(os.path.join(dldir, dlname + '.apk')):
        labels.add('in-apkpure')


def get_apk_from_huawei_app_gallery(appid, labels, dldir='repo'):
    if not shutil.which('apkeep'):
        print('apkeep not installed, skipping Huawei AppGallery')
        return
    dlname = appid
    subprocess.run(
        ['apkeep', '--download-source', 'huawei-app-gallery', '--app', dlname, dldir]
    )
    if os.path.exists(os.path.join(dldir, dlname + '.apk')):
        labels.add('in-huawei-app-gallery')


def run_cli_tool(cmd, cwd=None, timeout=600):
    """Run a command line tool with output, exceptions caught and printed"""
    print(Fore.GREEN + '# ', ' '.join(cmd) + Style.RESET_ALL)
    p = None
    try:
        p = subprocess.run(
            cmd,
            cwd=cwd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            timeout=timeout,
        )
    except (OSError, subprocess.TimeoutExpired) as e:
        p = subprocess.CompletedProcess(cmd, 0x8BADF00D)
        p.stdout = str(e).encode()
    if p and p.returncode != 0:
        print(
            Fore.LIGHTYELLOW_EX + p.stdout.decode() + Style.RESET_ALL,
            flush=True,
            file=sys.stderr,
        )
    return p


def get_modules():
    modules = []
    for f in glob.glob(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), 'modules', '*.*')
    ):
        if (
            f[-1] == '~'
            or not os.path.isfile(f)
            or not os.access(f, os.R_OK)
            or not os.access(f, os.X_OK)
        ):
            print(
                '\n===================================================================\n',
                'Skipping',
                f,
            )
        else:
            modules.append(f)
    return sorted(modules)


def setup_for_modules():
    if 'CI' not in os.environ or os.getuid() != 0:
        print(
            Fore.YELLOW
            + 'WARNING: not running in CI environment as root!'
            + Style.RESET_ALL
        )
        return

    cmd = [
        'bash',
        'fdroidserver/buildserver/provision-apt-get-install',
        'http://deb.debian.org/debian',
    ]
    run_cli_tool(cmd)

    apt_regex = re.compile(
        r'''\s*(?:#|//|/\*)\s*issuebot_apt_install\s*[:= ]\s*([^#]+?)\s*(?:\*/)?\s*\n'''
    )
    pip_regex = re.compile(
        r'''\s*(?:#|//|/\*)\s*issuebot_pip_install\s*[:= ]\s*([^#]+?)\s*(?:\*/)?\s*\n'''
    )
    for f in get_modules():
        print(
            '\n===================================================================\n',
            'Setting up',
            f,
        )
        with open(f) as fp:
            data = fp.read()
        apt = apt_regex.search(data)
        if apt:
            cmd = ['apt-get', 'install'] + apt.group(1).strip().split()
            run_cli_tool(cmd)
        pip = pip_regex.search(data)
        if pip:
            cmd = ['pip3', '--timeout', '100', '--retries', '10', 'install']
            cmd += pip.group(1).strip().split()
            run_cli_tool(cmd)


def print_exc_flush(title=None):
    if title:
        print(title)
    sys.stdout.flush()
    traceback.print_exc()
    sys.stderr.flush()


def append_to_applicationIds_data(
    job_id, issue_id, appid, datafiles, builds=[], successful_builds=[]
):
    global applicationIds_data
    if appid not in applicationIds_data:
        applicationIds_data[appid] = []
    applicationIds_data[appid].append(
        {
            'attemptedBuilds': builds,
            'jobId': job_id,
            'issueId': issue_id,
            'modules': datafiles,
            'successfulBuilds': successful_builds,
        }
    )


def append_to_sourceUrls_data(
    job_id, issue_id, sourceUrl, datafiles, builds=[], successful_builds=[]
):
    global sourceUrls_data
    if sourceUrl not in sourceUrls_data:
        sourceUrls_data[sourceUrl] = []
    sourceUrls_data[sourceUrl].append(
        {
            'attemptedBuilds': builds,
            'jobId': job_id,
            'issueId': issue_id,
            'modules': datafiles,
            'successfulBuilds': successful_builds,
        }
    )


def process_issue(project, issue):
    """Run all the steps on an issue"""

    global PROCESSED

    print('=================================================================')
    print(issue.get_id(), issue.title)
    print('------------------------------------------------------------------')
    labels = set(issue.labels)
    if 'fdroid-bot' in labels:
        print('Skipping %s (already has fdroid-bot label).' % issue.title)
        return
    PROCESSED += 1
    labels.add('fdroid-bot')

    discovered_appids = set()
    gplay_urls = set()
    for m in GPLAY_PATTERN.finditer(issue.description):
        gplay_urls.add(m.group(0))
    for url in gplay_urls:
        discovered_appids.add(parse_qs(urlparse(url).query)['id'][0])

    for appid in APPLICATION_ID_PATTERN.findall(issue.description):
        discovered_appids.add(appid)

    git_urls = parse_git_urls_from_description(issue.description)
    for git_url in git_urls:
        labels.add('git-url')
        run_cli_tool(['fdroid', 'import', '--url', git_url, '--omit-disable'])
        get_apk_from_github(git_url, labels)

    appids = download_apks_urls_in_description(issue.description)
    if appids:
        discovered_appids.update(appids)

    if len(discovered_appids) == 1 and os.getenv('ISSUEBOT_PROCESS_WITHOUT_SOURCE'):
        # create a blank metadata file to let fdroidserver process these APKs
        metadata_file = get_metadata_file(list(discovered_appids)[0])
        if not os.path.exists(metadata_file):
            os.makedirs(os.path.dirname(metadata_file), exist_ok=True)
            with open(metadata_file, 'w') as fp:
                d = {
                    'MaintainerNotes': 'placeholder generated by issuebot',
                    'Name': issue.title,
                }
                yaml.dump(d, fp)

    metadata.warnings_action = None
    apps = metadata.read_metadata()
    if len(git_urls) > 1:
        labels.add('too-many-git-urls')
    if len(discovered_appids) > 1:
        labels.add('too-many-ApplicationIds')

    appid = None
    for current, app in apps.items():
        if app.get('SourceCode') in git_urls and len(git_urls) == 1:
            appid = current
            os.environ['ISSUEBOT_CURRENT_SOURCE_URL'] = app['SourceCode']
            break
        elif len(discovered_appids) == 1 and current in discovered_appids:
            appid = current
            break

    process_issue_or_merge_request(project, issue, appid, git_urls, labels)


def process_issue_or_merge_request(
    project, issue, appid=None, git_urls=[], labels=set(), builds=[]
):
    """Run all the steps on an issue or merge request

    This will try to make APKs available, either by building the
    entries in the 'builds' parameter, or by rsyncing from an official
    mirror.  This is useful for testing things like Fastlane support.

    The name "issue" is currently stuck because this was first
    developed only around issues.  Now, an "issue" can be an instance
    of either gitlab.v4.objects.ProjectIssue or
    gitlab.v4.objects.ProjectMergeRequest.  Duck typing makes it work,
    since the basic API is the same between them.  One caveat is that
    they are treated differently in regards to permissions.  For
    example, GitLab "Reporter" status gives access to label issues,
    but not merge requests.

    """
    datafiles = None
    issue_id = int(issue.get_id())
    jobId = os.getenv('CI_JOB_ID')
    if not jobId:
        os.environ['CI_JOB_ID'] = datetime.utcnow().strftime('%s')
    successful_builds = []

    labels.update(issue.labels)

    if appid:
        if isinstance(issue, gitlab.v4.objects.ProjectMergeRequest):
            metadata.warnings_action = None
            apps = metadata.read_metadata()
            os.environ['ISSUEBOT_CURRENT_SOURCE_URL'] = apps[appid]['SourceCode']

        if builds:
            for build in builds:
                # fdroid build --on-server expects sudo is installed, then uninstalls it
                p = run_cli_tool(['apt-get', 'install', 'sudo'])
                print(p.stdout.decode())
                p = run_cli_tool(
                    ['fdroid', 'fetchsrclibs', '--verbose', build], timeout=3600
                )
                print(p.stdout.decode())
                p = run_cli_tool(
                    ['fdroid', 'build', '-v', '--on-server', '--no-tarball', build],
                    timeout=3600,
                )
                print(p.stdout.decode())
            for f in glob.glob('unsigned/%s_*.apk' % appid):
                successful_builds.append(f)
        else:
            # run fdroid scanner as a way to checkout the source code
            p = run_cli_tool(['fdroid', 'scanner', '--quiet', '-Wignore'], timeout=600)
            print(p.stdout.decode())

        _emoji, _labels, report = run_per_app(issue_id, appid)
        metadata_file = get_metadata_file(appid)
        datafiles = [
            f[16:]
            for f in glob.glob(
                os.path.join(ISSUEBOT_API_DIR, str(jobId), str(issue_id), '*.json')
            )
        ]
        labels.update(_labels)
        try:
            project.labels.create({'name': appid, 'color': '#eee'})
        except gitlab.exceptions.GitlabCreateError as e:
            print(appid, e)
        labels.add(appid)
        append_to_applicationIds_data(
            jobId, issue_id, appid, datafiles, builds, successful_builds
        )

        if successful_builds:
            if len(successful_builds) == len(builds):
                labels.add('builds')
                report += '\n<h1><tt>%s</tt> Builds!</h1><ul>' % appid
            else:
                report += '\n<h1><tt>%s</tt>Some builds succeeded</h1><ul>' % appid
            for f in successful_builds:
                report += '<li><a href="{project_url}/-/jobs/{jobId}/artifacts/raw/{f}">{f}</a></li>'.format(
                    project_url=os.getenv('CI_PROJECT_URL'), jobId=jobId, f=f
                )
            report += '</ul>\n\n'

        if os.getenv('CI_PROJECT_NAME') == 'rfp':
            if successful_builds:
                report += (
                    'This metadata file builds (_%s_):\n```yaml\n'
                    % get_metadata_file(appid)
                )
                with open(metadata_file) as fp:
                    report += fp.read()
                report += '\n```\n\n'

            fdroiddata_url = (
                'https://gitlab.com/fdroid/fdroiddata/-/commits/master/'
                + get_metadata_file(appid)
            )
            r = requests_head(fdroiddata_url)
            if r.status_code == 200:
                report += (
                    'Closing issue, <tt>%s</tt> already in included in fdroiddata:\n%s'
                    % (appid, fdroiddata_url)
                )
                labels.add('in-fdroiddata')
                issue.state_event = 'close'

        if report:
            if os.getenv('FROM_CI_JOB_ID'):
                # triggered by trigger-issuebot job from the issue or fork
                header = '<h2><tt>%s</tt></h2>' % (appid)
                tf = '<p><small>triggered by <a href="%s/-/jobs/%s"><tt>%s</tt></a></small></p>'
                header += tf % (
                    os.getenv('FROM_CI_PROJECT_URL'),
                    os.getenv('FROM_CI_JOB_ID'),
                    os.getenv('FROM_CI_COMMIT_SHA'),
                )
            elif os.getenv('FROM_CI_COMMIT_SHA'):
                # triggered by the scheduled job in fdroid/fdroiddata
                header = '<p><small>triggered by <tt>%s</tt></small></p>' % (
                    os.getenv('FROM_CI_COMMIT_SHA')
                )
            else:
                header = ''
            header += (
                '<p><small>generated by <a href="%s/-/jobs/%s">GitLab CI Job #%s</a></small></p>'
                % (os.getenv('CI_PROJECT_URL'), jobId, jobId)
            )
            report = header + report
            with open(
                'public/%s-%s-%s-report.txt' % (jobId, issue_id, appid), 'w'
            ) as fp:
                fp.write(report)
            issue.notes.create({'body': report[:1000000]})

        if not git_urls and os.path.isfile(metadata_file):
            try:
                with open(metadata_file) as fp:
                    data = yaml.load(fp, Loader=SafeLoader)
                if data and data.get('SourceCode'):
                    git_urls = [data['SourceCode']]
            except (
                FileNotFoundError,
                yaml.scanner.ScannerError,
                yaml.reader.ReaderError,
            ) as e:
                print(Fore.YELLOW + 'WARNING: ' + str(e) + Style.RESET_ALL)

    if labels:
        issue.labels = sorted(labels)

    try:
        issue.save()
    except gitlab.exceptions.GitlabUpdateError as e:
        if isinstance(issue, gitlab.v4.objects.ProjectMergeRequest):
            print(
                Fore.YELLOW
                + 'WARNING: "'
                + str(e)
                + '": issuebot probably failed to post labels since Developer '
                + 'status is required to post labels to merge requests:\n'
                + Style.RESET_ALL
            )
        else:
            raise (e)

    for git_url in git_urls:
        append_to_sourceUrls_data(jobId, issue_id, git_url, builds, successful_builds)

    p = run_cli_tool(
        ['fdroid', 'update', '--rename-apks', '--create-metadata', '--nosign']
    )
    print(p.stdout.decode())
